#include<iostream>
#include<cstdlib>
using namespace std;

int getmax(int array[],int length)
{
    int sum = 0;
    int max = 0;   
    int startIndex = 0; 
    int endIndex = 0;   
    int newStartIndex = 0; 
    for (int i = 0; i < length; i++)   
    {
        if (max < 0)   
        {
            max = array[i];    
            newStartIndex = i;  
        }
        else
        {
            max += array[i];  
        }
        if (sum < max) 
        {
            sum = max; 
            startIndex = newStartIndex; 
            endIndex = i;  
        }
    }
    return max;
}

int getstartIndex(int array[],int length)
{
    int sum = 0;   
    int max = 0;  
    int startIndex = 0; 
    int endIndex = 0;   
    int newStartIndex = 0; 
    for (int i = 0; i < length; i++)   
    {
        if (max < 0)  
        {
            max = array[i];    
            newStartIndex = i; 
        }
        else
        {
            max += array[i];   
        }
        if (sum < max) 
        {
            sum = max;
            startIndex = newStartIndex; 
            endIndex = i;   
        }
    }
    return startIndex;
}
int getendIndex(int array[],int length)
{
    int sum = 0;   
    int max = 0;   
    int startIndex = 0; 
    int endIndex = 0;   
    int newStartIndex = 0;  
    for (int i = 0; i < length; i++)   
    {
        if (max < 0)  
        {
            max = array[i];    
            newStartIndex = i;  
        }
        else
        {
            max += array[i];   
        }
        if (sum < max) 
        {
            sum = max; 
            startIndex = newStartIndex; 
            endIndex = i;   
        }
    }
    return endIndex;
}


int main()
{
    int length,i=0;
    cout<<"请输入个数：";
    cin>>length;
    cout<<"请输入数组：";
    int array[1000]={0};
    for(i=0;i<length;i++)
    {
        cin>>array[i];
    }
        cout<<"最大子数组的和为："<<getmax(array,length)<<endl;
    cout<<"最大子数组起始下标："<<getstartIndex(array,length)<<endl;
    cout<<"最大子数组结束下标："<<getendIndex(array,length)<<endl;
    system("pause");
    return 0;
}